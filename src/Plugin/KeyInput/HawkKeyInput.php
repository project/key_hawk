<?php

namespace Drupal\key_hawk\Plugin\KeyInput;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyInputBase;

/**
 * Defines a key input that provides a fields for a Dumbo connection.
 *
 * @KeyInput(
 *   id = "hawk",
 *   label = @Translation("Hawk authentication"),
 *   description = @Translation("A collection of fields to store Hawk HTTP authentication credentials.")
 * )
 */
class HawkKeyInput extends KeyInputBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'key' => '',
      'id' => '',
      'algorithm' => 'sha256',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $key = $form_state->getFormObject()->getEntity();
    $definition = $key->getKeyType()->getPluginDefinition();
    $fields = $definition['multivalue']['fields'];

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hawk Authetication Key'),
      '#required' => $fields['key']['required'],
      '#maxlength' => 4096,
      '#default_value' => $config['key'],
      '#attributes' => ['autocomplete' => 'off'],
    ];

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hawk ID value'),
      '#required' => $fields['id']['required'],
      '#maxlength' => 4096,
      '#default_value' => $config['id'],
      '#attributes' => ['autocomplete' => 'off'],
    ];

    $algorithm_options = [
      'sha1' => 'SHA-1',
      'sha256' => 'SHA-256',
    ];

    $form['algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('HMAC algorithm'),
      '#required' => $fields['algorithm']['required'],
      '#options' => $algorithm_options,
      '#default_value' => $config['algorithm'],
      '#attributes' => ['autocomplete' => 'off'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processSubmittedKeyValue(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $processed_values = [
      'submitted' => $values,
      'processed_submitted' => $values,
    ];

    return $processed_values;
  }
}
