<?php

namespace Drupal\key_hawk\Plugin\KeyType;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key for Hawk credentials.
 *
 * @KeyType(
 *   id = "hawk_key",
 *   label = @Translation("Hawk"),
 *   description = @Translation("Hawk credentials"),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "hawk"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "id" = {
 *          "label" = @Translation("Hawk ID Value"),
 *          "required" = true,
 *       },
 *       "key" = {
 *          "label" = @Translation("Hawk Authentication Key"),
 *          "required" = true,
 *       },
 *       "algorithm" = {
 *          "label" = @Translation("HMAC Algorithm"),
 *          "required" = true,
 *       },
 *     }
 *   }
 * )
 */

class HawkKeyType extends AuthenticationMultivalueKeyType {
  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return Json::encode($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!is_array($key_value) || empty($key_value)) {
      return;
    }

    $input_fields = $form['settings']['input_section']['key_input_settings'];

    $definition = $this->getPluginDefinition();
    $fields = $definition['multivalue']['fields'];

    foreach ($fields as $id => $field) {
      if (!is_array($field)) {
        $field = ['label' => $field];
      }

      if (isset($field['required']) && $field['required'] === FALSE) {
        continue;
      }

      $error_element = isset($input_fields[ $id ]) ? $input_fields[ $id ] : $form;

      if (!isset($key_value[$id])) {
        $form_state->setError($error_element, $this->t('The key value is missing the field %field.', ['%field' => $id]));
      } elseif (empty($key_value[$id])) {
        $form_state->setError($error_element, $this->t('The key value field %field is empty.', ['%field' => $id]));
      }
    }
  }
}
