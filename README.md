## INTRODUCTION

Key Hawk is an extension to the Key module and provides a new
key type and supporting inputs to easily add a key containing
credentials for an [Hawk HTTP Authorization](https://github.com/mozilla/hawk) key.

## REQUIREMENTS

This module is tested only on Drupal 9.0 and above

## INSTALLATION

Install key_hawk using a standard method for installing a contributed Drupal
module, either by downloading the package or using composer with
`composer require drupal/key_hawk`

## USING A KEY

Follow the [Key](https://drupal.org/project/key) documentation for getting and using keys
and adding a `key_select` element to your form in order to select your key,
you can spice the select box by adding a filter to select only hawk keys:


```
$form['hawk_auth'] = [
  '#type' => 'key_select',
  '#title' => $this->t('Hawk Authorization'),
  '#key_filters' = ['type' => 'hawk_key']
];
```
